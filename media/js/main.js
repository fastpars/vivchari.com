jQuery(document).ready(function($) {
	var
		galleryItems = $('.gallerySelect > .item'),
		galleryLastItem = galleryItems.length-1,
		galleryVisibleItems = 8,
		gallerySelectPosition = 0,
		gallerySelectMaxPosition = galleryItems.length - galleryVisibleItems,
		gallerySelectedItem = 0
		galleryBgLater = 0
		sliderTimeout = -1;

	$('.gallerySelectWrap').on('click', '.next', function() {
		if(gallerySelectPosition >= gallerySelectMaxPosition) {
			return;
		}
		galleryItems.eq(gallerySelectPosition).hide();
		gallerySelectPosition++;
	});

	$('.gallerySelectWrap').on('click', '.prev', function() {
		if(gallerySelectPosition == 0) {
			return;
		}
		gallerySelectPosition--;
		galleryItems.eq(gallerySelectPosition).show();
	});

	$('.gallerySelect').on('click', '.item', function() {
		var th = $(this);
		var bg = getGalleryBigImg(th);
		gallerySetBG(bg);
		gallerySelectedItem = th.index();
		setd300d400(th);
	});


	$('.galleryImg').on('click', '.forward', function() {
		if(gallerySelectedItem == galleryLastItem ) {
			return;
		}
		gallerySelectedItem++;
		var item = $('.gallerySelect > .item').eq(gallerySelectedItem);
		var bg = getGalleryBigImg(item);
		setd300d400(item);
		gallerySetBG(bg);

		if(gallerySelectedItem > gallerySelectPosition+galleryVisibleItems-1) {
			galleryItems.eq(gallerySelectPosition).hide();
			gallerySelectPosition++;
		}
	});

	$('.galleryImg').on('click', '.back', function() {
		if(gallerySelectedItem == 0) {
			return;
		}
		gallerySelectedItem--;
		var item = $('.gallerySelect > .item').eq(gallerySelectedItem);
		var bg = getGalleryBigImg(item);
		setd300d400(item);
		gallerySetBG(bg);

		if(gallerySelectPosition > gallerySelectedItem) {
		gallerySelectPosition--;
		galleryItems.eq(gallerySelectPosition).show();
		}
	});

	$('.slider').on('click', '.navigate > div', function() {
		setSlider($(this).index());
	});

	if($('.slider').length) {
		animateSlider();
	}

});

function setd300d400(el) {
	if(el.hasClass('d400')) {
		$('.description > .d300').hide();
		$('.description > .d400').show();
	} else if(el.hasClass('d300')) {
		$('.description > .d400').hide();
		$('.description > .d300').show();
	}
}

function gallerySetBG(bg) {
	var prevBg = galleryBgLater;
	galleryBgLater = galleryBgLater === 0? 1:0;

	var bgEl = $('.galleryImg .layer'+galleryBgLater);
	bgEl
		.css('opacity', 0)
		.css('z-index', 1)
		.css('background-image', bg);

	$('.galleryImg .layer'+prevBg)
		.css('z-index', 0);

	bgEl.animate({opacity:1}, 200);

}

function getGalleryBigImg(jqEl) {
	return (jqEl.children('div').css('background-image')).replace('gallery_thumbnal', 'gallery');
}

var sliderIndex = 0,
	sliderLayer = 0;

	var sliderHTML= {
		ua: [
			{
				title:"ЕТНО-КОТТОН<br>1200",
				href:"ua-product-ethno-cotton-1200.html"
			},
			{
				title:"ЕТНО-КОТТОН<br>1500",
				href:"ua-product-ethno-cotton-1500.html"
			},
			{
				title:"ЕТНО-НАТУРА",
				href:"ua-product-ethno-natura.html"
			},
			{
				title:"НАПІВШЕРСТЬ",
				href:"ua-product-semi-wool.html"
			}
		],
		ru: [
			{
				title:"ETHNO-COTTON<br>1200",
				href:"ru-product-ethno-cotton-1200.html"
			},
			{
				title:"ETHNO-COTTON<br>1500",
				href:"ru-product-ethno-cotton-1500.html"
			},
			{
				title:"ETHNO-NATURA",
				href:"ru-product-ethno-natura.html"
			},
			{
				title:"SEMI-WOOL",
				href:"ru-product-semi-wool.html"
			}
		],
	};

var indexRelation = [0, 1, 2, 3, 0, 1, 2, 3];

function setSlider(index) {
	sliderIndex = index;
	var bg = "url(media/img/slider/"+ sliderIndex +".jpg)";
	var prevBg = sliderLayer;
	sliderLayer = sliderLayer === 0? 1:0;

	var lang = $('body').hasClass('ua')? 'ua':'ru';
	var bgEl = $('.slider .layer'+sliderLayer);
	var html = sliderHTML[lang][indexRelation[sliderIndex]];

	bgEl
		.css('opacity', 0)
		.css('z-index', 1)
		.css('background-image', bg);

	bgEl.find('.btn').attr('href', html.href);
	bgEl.find('.title').html(html.title);

	$('.slider .layer'+prevBg)
		.stop()
		.css('z-index', 0);

	animateSlider();
	bgEl.stop().animate({opacity:1}, 1200, function() {
		var s = $('.slider .navigate > div');
		s.removeClass('active');
		s.eq(index).addClass('active');
	});
}

function animateSlider() {
	clearTimeout(sliderTimeout);
	sliderTimeout = setTimeout(function(){
		sliderIndex++;
		if(sliderIndex == 8) {
			sliderIndex = 0;
		}
		setSlider(sliderIndex);
	}, 5000);
}
